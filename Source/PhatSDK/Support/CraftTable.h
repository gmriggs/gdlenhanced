
#pragma once

template<typename TStatType, typename TDataType>
class TYPEMod : public PackObj, public PackableJson
{
public:
	using stat_list = PackableHashTableWithJson<TStatType, TDataType>;

	virtual void Pack(class BinaryWriter *pWriter) override; 
	virtual bool UnPack(class BinaryReader *pReader) override;
	virtual void PackJson(json& writer) override;
	virtual bool UnPackJson(const json& reader) override;

	virtual bool apply(stat_list *target, stat_list *source, stat_list *result)
	{
		TDataType *tval = nullptr;
		if (target)
		{
			tval = target->lookup(_stat);
		}
		TDataType *sval = nullptr;
		if (source)
		{
			sval = source->lookup(_stat);
		}
		TDataType *rval = nullptr;
		if (result)
		{
			rval = result->lookup(_stat);
		}

		switch (_operationType)
		{
		case 1: //=
			if (tval) *tval = _value;
			else target->add(_stat, &_value);
			return true;
		case 2: //+
			if (tval) *tval = *tval + _value;
			else target->add(_stat, &_value);
			return true;
		case 3: //source -> target
			if (sval)
			{
				if (tval) *tval = *sval;
				else target->add(_stat, sval);
			}
			return true;
		case 4: //source -> new
			if (sval)
			{
				if (rval) *rval = *sval;
				else result->add(_stat, sval);
			}
			return true;
		// 5, 6?
		//case 7: //add spell
		//	CSpellTable * pSpellTable = MagicSystem::GetSpellTable();
		//	if (_stat && pSpellTable && pSpellTable->GetSpellBase(intMod._stat))
		//		pTarget->m_Qualities.AddSpell(intMod._stat);
		//	return true;
		default:
			break;
		}

		return false;
	}

	int _unk;
	int _operationType;
	TStatType _stat;
	TDataType _value;
};

template<typename TStatType, typename TDataType>
class TYPERequirement : public PackObj, public PackableJson	
{
public:
	using quality_table_t = PackableHashTableWithJson<TStatType, TDataType>;

	virtual void Pack(class BinaryWriter *pWriter) override;
	virtual bool UnPack(class BinaryReader *pReader) override;
	virtual void PackJson(json& writer) override;
	virtual bool UnPackJson(const json& reader) override;

	virtual bool pass(quality_table_t *table)
	{
		TDataType *val = nullptr;
		if (table)
		{
			val = table->lookup(_stat);
		}

		switch (_operationType)
		{
		case 1: //> - unconfirmed
			return val && *val > _value;
		case 2: //>=
			return val && *val >= _value;
		case 3: //<
			return !val || *val < _value;
		case 4: //<=
			return !val || *val <= _value;
		case 5: //==
			return val && *val == _value;
		case 6: //!=
			return !val || *val != _value;
		case 7: //exists
			return val != nullptr;
		case 8: //doesnt exist
			return val == nullptr;
		// ??
		//case 9: //exists and != (or maybe just exists)
		//	if (exists && value != intRequirement._value)
		//	{
		//		SendText(intRequirement._message.c_str(), LTT_CRAFT);
		//		return false;
		//	}
		//	break;
		default:
			break;
		}

		return false;
	}

	TStatType _stat;
	TDataType _value;
	int _operationType;
	std::string _message;
};

class CraftRequirements : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE()
	DECLARE_PACKABLE_JSON();

	virtual bool pass(CWeenieObject *actor, CWeenieObject *item)
	{
		if (!pass(actor, _intRequirement, item->m_Qualities.m_IntStats))
			return false;

		if (!pass(actor, _didRequirement, item->m_Qualities.m_DIDStats))
			return false;

		if (!pass(actor, _iidRequirement, item->m_Qualities.m_IIDStats))
			return false;

		if (!pass(actor, _floatRequirement, item->m_Qualities.m_FloatStats))
			return false;

		if (!pass(actor, _stringRequirement, item->m_Qualities.m_StringStats))
			return false;

		if (!pass(actor, _boolRequirement, item->m_Qualities.m_BoolStats))
			return false;

		return true;
	}

	template <typename stat_type_t, typename val_type_t>
	bool pass(CWeenieObject *actor, PackableListWithJson<TYPERequirement<stat_type_t, val_type_t>> &reqs, PackableHashTableWithJson<stat_type_t, val_type_t> *stats)
	{
		for (auto req : reqs)
		{
			if (!req.pass(stats))
			{
				actor->SendText(req._message.c_str(), LTT_CRAFT);
				return false;
			}
		}
		return true;
	}

	PackableListWithJson<TYPERequirement<STypeInt, int>> _intRequirement;
	PackableListWithJson<TYPERequirement<STypeDID, uint32_t>> _didRequirement;
	PackableListWithJson<TYPERequirement<STypeIID, uint32_t>> _iidRequirement;
	PackableListWithJson<TYPERequirement<STypeFloat, double>> _floatRequirement;
	PackableListWithJson<TYPERequirement<STypeString, std::string>> _stringRequirement;
	PackableListWithJson<TYPERequirement<STypeBool, BOOL>> _boolRequirement;
};

class CraftMods : public PackObj, public PackableJson
{
public:
	using weenie_ptr = CWeenieObject*;
	template <typename key_t, typename value_t>
	using type_mod_list = PackableListWithJson<TYPEMod<key_t, value_t>>;
	template <typename key_t, typename value_t>
	using stat_list = PackableHashTableWithJson<key_t, value_t>;

	virtual void Pack(class BinaryWriter *pWriter) override;
	virtual bool UnPack(class BinaryReader *pReader) override;
	virtual void PackJson(json& writer) override;
	virtual bool UnPackJson(const json& reader) override;

	virtual bool apply(weenie_ptr target, weenie_ptr source, weenie_ptr result, weenie_ptr actor)
	{
#define Q(obj, type) obj->m_Qualities.m_ ## type ## Stats
#define QInt(obj) Q(obj, Int)
#define QDID(obj) Q(obj, DID)
#define QIID(obj) Q(obj, IID)
#define QFloat(obj) Q(obj, Float)
#define QStr(obj) Q(obj, String)
#define QBool(obj) Q(obj, Bool)
		// api also needs result item
		// TYPEMod::apply may copy from tool -> target or tool -> result
		// not sure of proper flow
		bool success = true;

		success &= apply(actor, _intMod, QInt(target), QInt(source), QInt(result));
		success &= apply(actor, _didMod, QDID(target), QDID(source), QDID(result));
		success &= apply(actor, _iidMod, QIID(target), QIID(source), QIID(result));
		success &= apply(actor, _floatMod, QFloat(target), QFloat(source), QFloat(result));
		success &= apply(actor, _stringMod, QStr(target), QStr(source), QStr(result));
		success &= apply(actor, _boolMod, QBool(target), QBool(source), QBool(result));

		// no access to portalDatEx here
		// should this happen first or last?
		//if (_modificationScriptId)
		//{
		//	CMutationFilter *filter = g_pPortalDataEx->GetMutationFilter(_modificationScriptId);
		//	if (filter)
		//	{
		//		filter->TryMutate(target->m_Qualities, 0);
		//	}
		//}

		return success;
	}

	template <typename key_t, typename value_t>
	bool apply(weenie_ptr actor, type_mod_list<key_t, value_t> &mods,
		stat_list<key_t, value_t> *target, stat_list<key_t, value_t> *source, stat_list<key_t, value_t> *result)
	{
		for (auto mod : mods)
		{
			if (!mod.apply(target, source, result))
			{
				//actor->SendText(req._message.c_str(), LTT_CRAFT);
				return false;
			}
		}
		return true;
	}

	PackableListWithJson<TYPEMod<STypeInt, int>> _intMod;
	PackableListWithJson<TYPEMod<STypeDID, uint32_t>> _didMod;
	PackableListWithJson<TYPEMod<STypeIID, uint32_t>> _iidMod;
	PackableListWithJson<TYPEMod<STypeFloat, double>> _floatMod;
	PackableListWithJson<TYPEMod<STypeString, std::string>> _stringMod;
	PackableListWithJson<TYPEMod<STypeBool, BOOL>> _boolMod;

	int _ModifyHealth=0;
	int _ModifyStamina=0;
	int _ModifyMana=0;
	int _RequiresHealth=0;
	int _RequiresStamina=0;
	int _RequiresMana=0;

	bool _unknown7 = false;
	uint32_t _modificationScriptId = 0 ;

	int _unknown9 = 0;
	uint32_t _unknown10 = 0;
};

class CraftOperationData : public PackableJson
{
public:
	DECLARE_PACKABLE_JSON();

	uint32_t _unk = 0;
	STypeSkill _skill = STypeSkill::UNDEF_SKILL;
	int _difficulty = 0;
	uint32_t _SkillCheckFormulaType = 0;
	uint32_t _successWcid = 0;
	uint32_t _successAmount = 0;
	std::string _successMessage;
	uint32_t _failWcid = 0;
	uint32_t _failAmount = 0;
	std::string _failMessage;

	double _successConsumeTargetChance;
	int _successConsumeTargetAmount;
	std::string _successConsumeTargetMessage;

	double _successConsumeToolChance;
	int _successConsumeToolAmount;
	std::string _successConsumeToolMessage;

	double _failureConsumeTargetChance;
	int _failureConsumeTargetAmount;
	std::string _failureConsumeTargetMessage;

	double _failureConsumeToolChance;
	int _failureConsumeToolAmount;
	std::string _failureConsumeToolMessage;

	CraftRequirements _requirements[3];
	CraftMods _mods[8];

	uint32_t _dataID = 0;
};

class CCraftOperation : public PackObj, public CraftOperationData
{
public:
	DECLARE_PACKABLE()

};

class JsonCraftOperation : public CraftOperationData
{
public:
	JsonCraftOperation() = default;
	JsonCraftOperation(const CraftOperationData& other)
		: CraftOperationData(other), _recipeID()
	{
	}
	JsonCraftOperation(const CraftOperationData& other, uint32_t id)
		: CraftOperationData(other), _recipeID(id)
	{
	}

	DECLARE_PACKABLE_JSON();

	uint32_t _recipeID = 0;
};

class CCraftTable : public PackObj, public PackableJson
{
public:
	CCraftTable();
	virtual ~CCraftTable() override;

	DECLARE_PACKABLE()
	DECLARE_PACKABLE_JSON();
	

	PackableHashTable<uint32_t, CCraftOperation> _operations;
	PackableHashTable<uint64_t, uint32_t, uint64_t> _precursorMap;
};

class CraftPrecursor : public PackableJson
{
public:
	CraftPrecursor() = default;
	CraftPrecursor(uint32_t recipe, uint32_t tool, uint32_t target) :
		RecipeID(recipe), Tool(tool), Target(target)
	{ }

	DECLARE_PACKABLE_JSON();

	uint32_t Tool = 0;
	uint32_t Target = 0;
	uint32_t RecipeID = 0;
	uint64_t ToolTargetCombo = 0;

};

